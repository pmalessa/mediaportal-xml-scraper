﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Mediaportal_XML_Scraper
{
    class Program
    {
        static void Main(string[] args)
        {
            String episode_key = "Dies ist die ";           //key to scan for episode number
            String series_key = " Episode der ";            //key to scan for series number
            byte[] stream = new byte[4096];
            String input;

            foreach (string argument in args)       //for every command line argument
            {
                if (argument.Length == 0) return;               //if no argument -> leave

                uint episode_num = 0;                           //final episode number
                uint series_num = 0;                            //final series number

                String dirPath = Path.GetDirectoryName(argument);   //get path without filename and extension
                String fileName = Path.GetFileNameWithoutExtension(argument);   //get filename without extension
                String filePath = dirPath + "\\" + fileName;

                if (!File.Exists(filePath + ".xml")) continue;  //if xml file doesnt exist -> next file
                FileStream fileStream = new FileStream(filePath + ".xml", FileMode.Open);   //open xml file
                try
                {
                    fileStream.Read(stream, 0, 4096);                               //read file
                    input = System.Text.Encoding.Default.GetString(stream);  //get String
                    String[] split = input.Split('.');          //split String at every '.'
                    foreach (String word in split)              //for each splitted String
                    {
                        int pos = word.IndexOf(episode_key);    
                        if (pos != -1)                          //if episode_key is inside this string
                        {
                            pos += episode_key.Length;          //shift position by key length
                            String sub = word.Substring(pos);   //get the episode_number
                            episode_num = (uint)Int32.Parse(sub);
                            break;
                        }
                    }
                    foreach (String word in split)              //for each splitted string
                    {
                        int pos = word.IndexOf(series_key);
                        if (pos != -1)                          //if series_key is inside this string
                        {
                            pos += series_key.Length;           //shift position by key length
                            String sub = word.Substring(pos);   //get the series_number
                            series_num = (uint)Int32.Parse(sub);
                            break;
                        }
                    }
                }
                finally
                {
                    fileStream.Close();
                }
                if (episode_num == 0 || series_num == 0) return;    //leave if no number found

                if (!File.Exists(filePath + ".ts")) continue;       //if ts file doesnt exist -> next file

                /*Insert the info to filename*/

                char[] splitkey = { '-' };                          //split .ts filename at every '-'
                String[] split2 = fileName.Split(splitkey, 3);      //but only 3 times
                if (split2[0] == "" || split2[1] == "" || split2[2] == "") return;  //leave if unsuccessful

                string oldFileName = filePath; // Full path of old file
                string newFileName = dirPath + "\\" + split2[0] + "- S" + series_num.ToString("00") + "E" + episode_num.ToString("00") + " -" + split2[2]; // insert episode and series number in file


                File.Move(oldFileName + ".ts", newFileName + ".ts");    //rename ts file
                File.Move(oldFileName + ".xml", newFileName + ".xml");  //rename xml file

            }
        }
    }
}
